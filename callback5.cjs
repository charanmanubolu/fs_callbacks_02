/* 
	Problem 5: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for the Mind and Space lists simultaneously
*/


const {boardID}=require('./callback1.cjs')
const {listOFIdsWithnoardsID} = require('./callback2.cjs')
const {cardsOFIdWithListID}=require('./callback3.cjs')

function problem5(id) {
    try{
        setTimeout(()=>{
            boardID("mcu453ed", (board) => {
           
                listOFIdsWithnoardsID(board.id, (lists) => {
                  const mindList = lists.find((list) => {
                    return list.name === "Mind" 
                  });
                  const SpaceList = lists.find((list) => {
                    return list.name==="Space";
                  });
          
                  cardsOFIdWithListID(mindList.id, (mindcards) => {
                      
                  
                  cardsOFIdWithListID(SpaceList.id, (spacecards) => {
                    console.log(board);
                    console.log(lists);
                    console.log(mindcards);
                    console.log(spacecards);
                });
                  });
                });
              });
        },2000)
        
      
    }catch(error){
        console.log(error)
    }
}



module.exports=problem5
