/* 
	Problem 4: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for the Mind list simultaneously
*/

const {boardID}=require('./callback1.cjs')
const {listOFIdsWithnoardsID} = require('./callback2.cjs')
const {cardsOFIdWithListID}=require('./callback3.cjs')

function problem4(id) {
    try{
        setTimeout(()=>{
            boardID("mcu453ed", (board) => {
           
                listOFIdsWithnoardsID(board.id, (lists) => {
                  const mindList = lists.find((list) => {
                    return list.name === "Mind";
                  });
          
                  cardsOFIdWithListID(mindList.id, (cards) => {
                    console.log(board);
                    console.log(lists);
                    console.log(cards);
                  });
                });
              });
        },2000)
        
      
    }catch(error){
        console.log(error)
    }
}



module.exports=problem4
