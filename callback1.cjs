/* 
    Problem 1: Write a function that will return a particular board's information based on the boardID that is passed 
    from the given list of boards in boards.json and then pass control back to the code that called it by using a callback function.
*/



const fs = require('fs')
const boardsFile = require('./boards_2.json')

function boardID(id, callback) {
    try {
        setTimeout(()=>{
            try{
        let result = boardsFile.find(Element => {
            if (Element.id === id) {
                return Element
            }
        })
        callback( result)
    }catch(err){
        console.log(err)
    }
    },2000)
        

    } catch (err) {
        console.log(err)
    }


}

function callback(err, data) {
    if (err) {
        console.log(err)
    } else {
        console.log(data)
    }
}


module.exports = { boardID, callback }

