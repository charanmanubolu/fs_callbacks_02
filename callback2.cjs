/* 
    Problem 2: Write a function that will return all lists that belong to a board based on the boardID that is passed to
     it from the given data in lists.json. Then pass control back to the code that called it by using a callback function.
*/

const fs = require('fs')
const listdata = require('./lists_1.json')

function listOFIdsWithnoardsID(id,  callback) {
    try {
        setTimeout(() => {
            let result = listdata[id]
            callback(result)
        }, 2000)
    }catch(err){
        console.log(err) 
    }
}

function callback(err, data) {
        if (err) {
            console.log(err)
        } else {
            console.log(data)
        }
    }


module.exports={listOFIdsWithnoardsID,callback}