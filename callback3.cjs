/* 
	Problem 3: Write a function that will return all cards that belong to a particular list based on the listID that is 
    passed to it from the given data in cards.json. Then pass control back to the code that called it by using a callback function.
*/


const fs = require('fs')
const cardsData = require('./cards_2.json')

function cardsOFIdWithListID(id,  callback) {
    try {
        setTimeout(() => {
            let result = cardsData[id]
            callback(result)
        }, 2000)
    }catch(err){
        console.log(err)
    }
}

function callback(err, data) {
        if (err) {
            console.log(err)
        } else {
            console.log(data)
        }
    }


module.exports={cardsOFIdWithListID,callback}